<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserPostsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $data = ['maxId' => 0, 'count' => 33, 'size' => 'thumbnail'];
        $builder->setMethod('GET')
            ->add('userId', IntegerType::class)
            ->add('maxId', IntegerType::class, ['data' => $data['maxId']])
            ->add('count', IntegerType::class, ['data' => $data['count']])
            ->add(
                'size',
                ChoiceType::class,
                array(
                'choices' => array('thumbnail' => 'thumbnail',
                'low_resolution' => 'low_resolution',
                'standard_resolution' => 'standard_resolution',
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'multiple' => false,
                'data' => $data['size'],
                )
            )->add('Поиск', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'csrf_protection' => false,
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'UserPostsType';
    }
}
