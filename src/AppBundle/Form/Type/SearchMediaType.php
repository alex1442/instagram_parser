<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;

class SearchMediaType extends AbstractType
{
    const BLOCK_PREFIX = 'SearchMediaType';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$data = ['maxTagId' => '0', 'count' => '33', 'size' => 'thumbnail'];
        $builder->setMethod('GET')
            ->add('tagName', SearchType::class)
           // ->add('maxTagId', IntegerType::class, ['empty_data' => $data['maxTagId']])
           // ->add('count', IntegerType::class, ['empty_data' => $data['count']])
            ->add('size', ChoiceType::class, [
                'choices' => array('thumbnail' => 'thumbnail',
                'low_resolution' => 'low_resolution',
                'standard_resolution' => 'standard_resolution',
                ),
                'multiple' => false,
               // 'empty_data' => $data['size'],
                ])->add('Поиск', SubmitType::class);

        if (0 and $options['attr']['flagCaptcha']) {
            $builder->add(
                'recaptcha',
                'ewz_recaptcha',
                array(
                'mapped' => false,
                'constraints' => array(
                new RecaptchaTrue(),
                ), )
            );
        }
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('size', 'standard_resolution');
        $resolver->setDefaults([
            'csrf_protection' => false,
            ]);
    }

    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
