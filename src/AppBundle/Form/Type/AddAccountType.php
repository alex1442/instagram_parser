<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AddAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $data = ['maxTagId' => 0, 'count' => 33, 'size' => 'thumbnail'];
        //dump($r->generateUrl('addAccount'));
        $builder
            ->setMethod('POST')
            ->add('instagramAccountId', IntegerType::class)
            ->add('caption', TextareaType::class, ['attr' => ['maxlength' => 255], 'required' => false])
            ->add(
                'activityStatus',
                CheckboxType::class,
                [
                'data' => true,
                'required' => false,
                ]
            )->add('Добавить в коллекцию', SubmitType::class);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'size' => 'thumbnail',
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'AddAccountType';
    }
}
