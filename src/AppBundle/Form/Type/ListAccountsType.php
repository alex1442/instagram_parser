<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ListAccountsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $data = ['activityStatus' => true, 'count' => 20, 'offset' => 0];
        $builder->setMethod('GET')
            ->add('q', null, ['required' => false])
            ->add(
                'activityStatus',
                CheckboxType::class,
                [
                    'required' => false,
                    'data' => $data['activityStatus'],
                ]
            )
            ->add('count', IntegerType::class, ['data' => $data['count']])
            ->add('Поиск', SubmitType::class)
            ->add('offset', HiddenType::class, ['data' => $data['offset']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'csrf_protection' => false,
            //  'data_class' =>
            )
        );
    }

    public function getBlockPrefix()
    {
        return 'ListAccountsType';
    }
}
