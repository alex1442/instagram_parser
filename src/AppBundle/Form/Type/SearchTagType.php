<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;

class SearchTagType extends AbstractType
{
    const BLOCK_PREFIX = 'SearchTagType';
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET')->add('tagName', SearchType::class)->add('Поиск', SubmitType::class);
        /*      if ($options['attr']['flagCaptcha']) {
            $builder->add(
                'recaptcha', 'ewz_recaptcha', ['mapped' => false,
                'constraints' => array(
                new RecaptchaTrue(),
                ),
                ]
            );
        }*/
    }
    public function getName()
    {
        return self::BLOCK_PREFIX;
    }
    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
