<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class GetVkRepostsType extends AbstractType
{
    const BLOCK_PREFIX = 'GetVkRepostsType';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$data = ['maxTagId' => '0', 'count' => '33', 'size' => 'thumbnail'];
        $builder->setMethod('GET')
            ->add('vkPostId', IntegerType::class, ['empty_data' => $data['maxTagId']])
            ->add('vkUserId', IntegerType::class, ['empty_data' => $data['vkUserId']])
            ->add('count', IntegerType::class, ['empty_data' => $data['count']])
            ->add('Поиск', SubmitType::class);

        if (0 and $options['attr']['flagCaptcha']) {
            $builder->add(
                'recaptcha',
                'ewz_recaptcha',
                array(
                'mapped' => false,
                'constraints' => array(
                new RecaptchaTrue(),
                ), )
            );
        }
    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('count', 20);
        $resolver->setDefaults([
            'csrf_protection' => false,
            ]);
    }

    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
