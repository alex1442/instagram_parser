<?php

namespace AppBundle\Service;

use MetzWeb\Instagram\Instagram;

//use InstagramScraper\Instagram;

class InstagramAPI extends Instagram
{
    private $_scopes = array('basic', 'likes', 'comments', 'relationships',    'public_content',
    'follower_list', );


    public function searchTags($name)
    {
        //dump($this->_makeCall('tags/search', false, array('q' => $name, 'access_token' => '2006303730.f2f08b3.d19d3b9d105942ff8bf2be749897c31d')));
        $requestAPI= $this->_makeCall('tags/search', true, array('q' => $name));
        function_exists('dump') ? dump($requestAPI) : null;
        if (isset($requestAPI->meta->code) and $requestAPI->meta->code === 200) {
            return $requestAPI->data;
        } else {
            throw new \Exception('search tag error '.serialize($requestAPI));
        }
    }
    
    public function getLoginUrl($scope = array('basic'))
    {
        if (is_array($scope) && count(array_intersect($scope, $this->_scopes)) === count($scope)) {
            return self::API_OAUTH_URL.'?client_id='.$this->getApiKey().'&redirect_uri='.urlencode($this->getApiCallback()).'&scope='.implode('+', $scope).'&response_type=code';
        } else {
            throw new \Exception("Error: getLoginUrl() - The parameter isn't an array or invalid scope permissions used.");
        }
    }

    public function getTagMedia($tagName, $count = 0, $maxTagId = 0)
    {
        $data['posts'] = [];
        $requestAPI = $this->requestGetTagMedia($tagName, $count, $maxTagId);
        function_exists('dump') ? dump($requestAPI) : null;
        if (isset($requestAPI->meta->code) and $requestAPI->meta->code === 200) {
            $count -= count($requestAPI->data);

            foreach ($requestAPI->data as $post) {
                $data['posts'][] = $post;
            }

            while (($count > 0) and $requestAPI = $this->pagination($requestAPI, $count)) {
                foreach ($requestAPI->data as $post) {
                    $data['posts'][] = $post;
                }
                $count -= count($requestAPI->data);
            }
            $data['nextMaxTagId'] = isset($requestAPI->pagination->next_max_tag_id) ? $requestAPI->pagination->next_max_tag_id : false;

            return $data;
        } else {
            throw new \Exception('search media error '.serialize($requestAPI));
        }
    }

    private function requestGetTagMedia($tagName, $count = 0, $maxTagId = 0)
    {
        $params = array();

        if ($count > 0) {
            $params['count'] = $count;
        }

        if ($maxTagId > 0) {
            $params['max_tag_id'] = $maxTagId;
        }
        return $this->_makeCall('tags/'.$tagName.'/media/recent', true, $params);
    }

    public function getUserMedia($userId = 'self', $count = 0, $maxId = 0)
    {
        $data['posts'] = [];
        $requestAPI = $this->requestGetUserMedia($userId, $count, $maxId);

        function_exists('dump') ? dump($requestAPI) : null;
        if (isset($requestAPI->meta->code) and $requestAPI->meta->code === 200) {
            $count -= count($requestAPI->data);

            foreach ($requestAPI->data as $post) {
                $data['posts'][] = $post;
            }
            while (($count > 0) and $requestAPI = $this->pagination($requestAPI, $count)) {
                foreach ($requestAPI->data as $post) {
                    $data['posts'][] = $post;
                }
                $count -= count($requestAPI->data);
            }
            $data['nextMaxId'] = isset($requestAPI->pagination->next_max_id) ? explode('_', $requestAPI->pagination->next_max_id)[0] : false;

            return $data;
        } else {
            throw new \Exception('search media error '.serialize($requestAPI));
        }
    }

    public function requestGetUserMedia($userId = 'self', $count = 0, $maxId = 0)
    {
        $params = array();

        if ($count > 0) {
            $params['count'] = $count;
        }

        if ($maxId > 0) {
            $params['max_id'] = $maxId;
        }

        return $this->_makeCall('users/'.$userId.'/media/recent', ($userId === 'self'), $params);
    }
}
