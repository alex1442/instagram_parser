<?php

namespace AppBundle\Service;

use ReCaptcha\ReCaptcha;

class ReCAPTCHA extends ReCaptcha
{
    private $request;
    public function verify($response, $remoteIp = null)
    {
        $request = parent::verify($response, $remoteIp);
        if ($request->isSuccess()) {
            return true;
        } else {
            return false;
        }//boolean
    }
}
