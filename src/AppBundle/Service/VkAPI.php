<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use VK\VK;

namespace AppBundle\Service;

/**
 * Description of VkAPI
 *
 * @author alex1442
 */
class VkAPI extends \VK\VK
{
    private $em;

    private $callbackUrl;
     


    public function __construct(int $appId, string $APISecret, string $callbackUrl, \Doctrine\ORM\EntityManager $em)
    {
          $this->em = $em;
          $this->callbackUrl=$callbackUrl;
         // $accessToken=//get from em
        return  parent::__construct($appId, $APISecret, $accessToken = null);
    }
  
    public function getAccessToken($code, $callbackUrl = null)
    {
        return parent::getAccessToken($code, isset($callbackUrl)? $callbackUrl :  $this->callbackUrl);
    }
    
    
    
    public function getAuthorizeUrl($apiSettings = '4530181', $callbackUrl = null, $testMode = false)
    {
        return parent::getAuthorizeUrl($apiSettings, isset($callbackUrl)? $callbackUrl :  $this->callbackUrl, $testMode);
    }
    
    //Возвращает репосты поста по ID, созданные не позднее $dueDate, если $dueDate=0 то не фильтрует по времени
    public function getAllRepostByPostId($postId, $dueDate = 0)
    {
        $post=  $this->api(
            'wall.getById',
            array(
                'posts'   => $postId
            )
        );
       
        $repostCount=$post["response"][0]["reposts"]["count"];
        $explodePostId=  explode("_", $postId, 2);
        $executeCode="var startOffset=0;        
        var maxPart = 1000;
        var requestReposts;
        var reposts = [];
        var countReposts = 500;
        var maxCountRequestReposts=25;

            var i = 1;
        var countRequests=parseInt((countReposts-startOffset)/maxPart)+1;
            while ((i <= maxCountRequestReposts) && (i<=countRequests) ) {
                requestReposts = API.wall.getReposts({
                    \"owner_id\": $explodePostId[0],
                    \"post_id\": $explodePostId[1],
                    \"count\": maxPart,
        \"offset\": startOffset+maxPart*(i-1)});
        //return requestReposts;
              reposts= reposts+ requestReposts.items;
        i=i+1;
            }
        return reposts;";
        $response= $this->api("execute", array("code"=>$executeCode,"access_token"=>"40bbf22a114a70a3baa893e180bb1ab7729c11b4b0fe97ace99affe9de4397699b502afaecc5bc00d999e"));
     
        return  array_filter($response["response"], function ($repost) use ($dueDate) {
            
            return !$dueDate ? true :($repost['date']<=$dueDate);
        });
    }
}
