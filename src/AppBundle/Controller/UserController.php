<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\UserPostsType;
use AppBundle\Entity\UserPostsEntity;
use AppBundle\Form\Type\AddAccountType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserController extends Controller
{
    /**
     * @Route("/user.{_format}/posts", name="userPosts")
     */
    public function userPostsAction(Request $request, $_format = 'html')
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults(
            [
                'page' => 1,
                'items' => 10,
            ]
        );
        $options = $resolver->resolve($input = []);
        function_exists('dump') ? dump($options) : null;

        $userPostsForm = $this->createForm(UserPostsType::class, $formData = new UserPostsEntity(), []);
        $userPostsForm->handleRequest($request);
        $form2 = $this->createForm(
            AddAccountType::class,
            null,
            [
                'action' => $this->get('router')->generate('addAccount'),
                'data' => [
                    'instagramAccountId' => $formData->getUserId(),
                ],
            ]
        );

        if ($userPostsForm->isValid()) {
            $data = $this->get('InstagramAPI')->getUserMedia($formData->getUserId(), $formData->getCount(), $formData->getMaxId());
            function_exists('dump') ? dump($data) : null;
        }
        $tplData = [
                    'title' => 'Посты пользоватетя',
                    'form' => $userPostsForm->createView(),
                    'form2' => $form2->createView(),
                    'size' => $formData->getSize(),
                    'posts' => isset($data['posts']) ? $data['posts'] : [],
                    'nextPageUrl' => (isset($data['nextMaxId'])  and
                        $data['nextMaxId'])
                    ? $this->get('router')->generate(
                        'userPosts',
                        [
                        '_format' => 'html',
                        $userPostsForm->getName() => ['userId' => $formData->getUserId(),
                        'maxId' => $data['nextMaxId'],
                        'count' => $formData->getCount(),
                        'size' => $formData->getSize(), ],
                        ]
                    ) : false,
                ];

        return $this->render(
            'user/userPosts.html.twig',
            $tplData
        );
    }
}
