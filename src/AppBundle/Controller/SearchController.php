<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\SearchTagType;
use AppBundle\Form\Type\SearchMediaType;
use AppBundle\Entity\SearchTagEntity;
use AppBundle\Entity\SearchMediaEntity;
use Symfony\Component\HttpFoundation\Cookie;
use InstagramScraper\Instagram;

class SearchController extends Controller
{
    /**
     * @Route("/search.{_format}", name="searchTag")
     */
    public function searchTagAction(Request $request, $format = 'html')
    {
        $VkAPI=$this->get('VkAPI');
        dump($VkAPI->getAllRepostByPostId('-22822305_155725'));
        //dump($account);
        //dump($medias);
        if ($format) {
        };
        //$flagCaptcha = $request->query->get('flagCaptcha');
        //$options = ['attr' => ['flagCaptcha' => (bool) $flagCaptcha]];
        $form = $this->createForm(SearchTagType::class, $tagEntity = new SearchTagEntity(), []);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $InstagramAPI=$this->get('InstagramAPI');
            $InstagramAPI->setAccessToken($request->cookies->get('instagramToken'));
            $tags = $InstagramAPI->searchTags($tagEntity->getTagName());
            function_exists('dump') ? dump($tags) : null;
        }

        $tplData = array(
                    'title' => 'Поиск тегов',
                    'form' => $form->createView(),
                    'tags' => isset($tags) ? $tags : [],
                    'prefix' => SearchMediaType::BLOCK_PREFIX,
                    );

        return $this->render(
            'AppBundle:search:index.html.twig',
            $tplData
        );
    }

    /**
     * @Route("/search.{_format}/media",
     *  name="searchMedia",
     *  requirements= {
     *  "_format":"html|json"
     * },
     *  defaults={
     * "_format":"html"
     * }
     * )
     */
    public function searchMediaAction(Request $request, $format = 'html')
    {
        if ($format) {
        }
        $form = $this->createForm(SearchMediaType::class, $formData = new SearchMediaEntity(), []);
        $form->handleRequest($request);
        if ($form->isValid()) {
                        $InstagramAPI=$this->get('InstagramAPI');
            $InstagramAPI->setAccessToken($request->cookies->get('instagramToken'));
            $data = $InstagramAPI
                ->getTagMedia(
                    $formData->getTagName(),
                    $formData->getCount(),
                    $formData->getMaxTagId()
                );
            function_exists('dump') ? dump($data) : null;
        }

        $tplData = [
                    'title' => 'Поиск постов',
                    'form' => $form->createView(),
                    'size' => $formData->getSize(),
                    'posts' => isset($data['posts']) ? $data['posts'] : [],
                    'nextPageUrl' => (isset($data['nextMaxTagId'])  and
                        $data['nextMaxTagId'])
                    ? $this->get('router')->generate(
                        'searchMedia',
                        ['_format' => 'html',
                        $form->getName() => ['tagName' => $formData->getTagName(),
                        'maxTagId' => $data['nextMaxTagId'],
                        'count' => $formData->getCount(),
                        'size' => $formData->getSize(),
                        ],
                        ]
                    ) : false,
            ];

        return $this->render(
            'AppBundle:search:searchMedia.html.twig',
            $tplData
        );
    }
}
/*

 *  var maxPart = 100;
var maxTime=200000000;
var requestReposts;
var reposts = [];
var countReposts = API.wall.getById({
    "posts": "-22822305_155725"
})[0].reposts.count;
if (countReposts < maxPart) {
    requestReposts = API.wall.getReposts({
        "owner_id": "-22822305",
        "post_id": 155725,
        "count": countReposts
    });
} else {
    var i = 1;
    if (i <= 23) {
        requestReposts = API.wall.getReposts({
            "owner_id": "-22822305",
            "post_id": 155725,
            "count": 1000
        });
        var j = 0;
        while (requestReposts.items[j]) {
            reposts.push(requestReposts.items[j].from_id);
            j = j + 1;
        }
        i = i + 1;
    }
}
return reposts;
 //return  requestReposts.items@.from_id;		//a.items[0].reposts;
*/
