<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\InstagramTokens;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\VkTokens;

class VkAuthController extends Controller
{
    /**
      * @Route("/vkauth.{_format}", name="vkAuth")
      */
    public function vkAuth(Request $request, $_format)
    {
     //($app_id, $api_secret, $access_token = null);
        $vk = $this->get('VkAPI');
        
      
        if (is_string($request->query->get('code'))) {
            dump($vkTokenResponse = $vk->getAccessToken($request->query->get('code')));
            if ($vkTokenResponse['access_token']) {
                $em = $this->getDoctrine()->getManager();

                    $newVkToken = new VkTokens();
                    $newVkToken->setAccessToken($vkTokenResponse['access_token'])->setUserId($vkTokenResponse['user_id'])
                            ->setExpiresIn($vkTokenResponse['expires_in'])->setEmail($vkTokenResponse['email']);
                    
                    $em->persist($newVkToken);
                
            
                $em->flush();
            } else {
                new \Exception($vkTokenResponse);
            }
        } else {
            $url= $vk->getAuthorizeUrl();
            $tplData =
            [
              'title' => 'login vk',
              'vk_auth_url' => $url,
            ];

            return $this->render(
                'AppBundle:vk_auth:vk_auth.html.twig',
                $tplData
            );
        }
    }
}
