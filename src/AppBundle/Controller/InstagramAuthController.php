<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\InstagramTokens;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class InstagramAuthController extends Controller
{
    /**
      * @Route("/instagramauth.{_format}", name="instagramAuth")
      */
    public function instagramAuth(Request $request, $_format)
    {
        $response = new Response('', null, $headers = array());
        if (is_string($request->query->get('code'))) {
            $tokenResponse = $this->get('InstagramAPI')->getOAuthToken($request->query->get('code'));
            dump($tokenResponse);
            if (isset($tokenResponse->access_token)) {
                $em = $this->getDoctrine()->getManager();
    
                $instagramToken = $em->getRepository('AppBundle:InstagramTokens')->findOneBy(
                    array('clientId' => $tokenResponse->user->id)
                );
                dump($instagramToken);

                if ($instagramToken) {
                      $instagramToken->setAccessToken($tokenResponse->access_token);
                } else {
                    $newInstagramToken = new InstagramTokens();
                    $newInstagramToken->setAccessToken($tokenResponse->access_token)->setClientId($tokenResponse->user->id);
                    $em->persist($newInstagramToken);
                }
            
                $em->flush();
                $response->headers->set('Location', '/app_dev.php');
                $response->headers->setCookie(new Cookie('instagramToken', $tokenResponse->access_token));
                return $response->send();
            } else {
                $response->headers->set('Location', $this->generateUrl(
                    'instagramAuth',
                    array('_format' =>$_format)
                ));
                return $response->send();
            }
        } else {
            $url = $this->get('InstagramAPI')->getLoginUrl(
                array('basic',
                'public_content',
                'follower_list',
                'comments',
                'relationships',
                'likes',
                )
            );

            $tplData =
            [
              'title' => 'login instagram',
              'instagram_auth_url' => $url,
            ];

            return $this->render(
                'AppBundle:instagram_auth:instagram_auth.html.twig',
                $tplData,
                $response
            );
        }
    }
}
