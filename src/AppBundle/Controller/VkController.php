<?php

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\Type\GetVkRepostsType;
use AppBundle\Entity\GetVkRepostsEntity;

namespace AppBundle\Controller;

class VkController
{
      /**
     * @Route("/vk_reposts.{_format}",
     *  name="vkReposts",
     *  requirements= {
     *  "_format":"html|json"
     * },
     *  defaults={
     * "_format":"html"
     * }
     * )
     */
    public function getVkRepostsAction(Request $request, $_format)
    {
        $form = $this->createForm(GetVkRepostsType::class, $formData = new GetVkRepostsEntity(), []);
        $form->handleRequest($request);
        if ($form->isValid()) {
        }
              $tplData = [
                  'title' => 'Получить репосты поста',
                  'form' => $form->createView(),
                  'size' => $formData->getSize(),
                  'nextPageUrl' => (isset($data['nextMaxTagId'])  and
                      $data['nextMaxTagId'])
                  ? $this->get('router')->generate(
                      'searchMedia',
                      ['_format' => 'html',
                      $form->getName() => ['tagName' => $formData->getTagName(),
                      'maxTagId' => $data['nextMaxTagId'],
                      'count' => $formData->getCount(),
                      'size' => $formData->getSize(),
                      ],
                      ]
                  ) : false,
              ];

              return $this->render(
                  'AppBundle:vk:getVkReposts.html.twig',
                  $tplData
              );
    }
}
