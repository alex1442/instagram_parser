<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\InstagramAccounts;
use AppBundle\Form\Type\AddAccountType;
use AppBundle\Entity\ListAccountsEntity;
use AppBundle\Form\Type\ListAccountsType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionController extends Controller
{
    /**
    * @Route("/collection.{_format}/add", name="addAccount")
    */
    public function addAccountAction(Request $request, $_format = 'html')
    {
        $form = $this->createForm(AddAccountType::class, $instagramAccount = new InstagramAccounts());
        $form->handleRequest($request);
        $tplData = [
            'title' => 'Сохранить аккаунт в коллекцию',
            'form' => $form->createView(),
        ];

        if ($form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($instagramAccount);
                $em->flush();
                $tplData['accountId'] = $instagramAccount->getAccountId();
            } catch (\PDOException $e) {
                $tplData['errorMsg'] = 'Error Insert';
            }
        }

        return $this->render(
            'AppBundle:collection:collectionAdd.html.twig',
            $tplData
        );
    }
    /**
    * @Route("/collection.{_format}/accounts", name="listAccounts")
    */
    public function listAccountsAction(Request $request, $_format = 'html')
    {
        $form = $this->createForm(ListAccountsType::class, $formData = new ListAccountsEntity());
        $form->handleRequest($request);
        $resolver = new OptionsResolver();
        $resolver->setDefaults(
            [
                'count' => 20,
                'activityStatus' => true,
                'offset' => 0,

            ]
        );
        if ($form->isValid()) {
            $input   = [
                'count' => $formData->getCount(),
                'activityStatus' => $formData->getActivityStatus(),
                'offset' => $formData->getOffset(),
            ];
            $options = $resolver->resolve($input);
            $em      = $this->getDoctrine()->getManager();
            $dql     = 'SELECT a FROM AppBundle:InstagramAccounts a';
            $query   = $em->createQuery($dql);
            $data    = $query->setMaxResults($options['count'])->setFirstResult($options['offset'])->getResult();
            function_exists('dump') ? dump($data) : null;
        }
        $tplData = [
            'title' => 'Сохранить аккаунт в коллекцию',
            'form' => $form->createView(),
            'accounts' => $data ?? null,
        ];

        return $this->render(
            'AppBundle:collection:listAccounts.html.twig',
            $tplData
        );
    }
    /**
    * @Route("/collection.{_format}/post/add", name="listAccounts")
    */
    public function addPostAction()
    {
    }
}
