<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchControllerTest extends WebTestCase
{
    public function testSearchTag()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/search.web');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Поиск тегов', $crawler->filter('title')->text());
    }
}
