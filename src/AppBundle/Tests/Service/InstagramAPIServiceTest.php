<?php

namespace AppBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InstagramAPIServiceTest extends WebTestCase
{
    public function testLoadableClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $container = $kernel->getContainer();
        $i = $container->get('InstagramAPI');
        $this->assertEquals(
            true,
            (get_class($i) === "AppBundle\Service\InstagramAPI")
            and "MetzWeb\Instagram\Instagram" === get_parent_class($i)
        );
    }
}
