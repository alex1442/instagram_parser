<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class UserPostsEntity
{
    /**
     *   @Assert\NotBlank()
     */
    protected $maxId;

    /**
     *   @Assert\Type(type="integer")
     */
    protected $userId;

    /**
     *   @Assert\Type(type="integer")
     */
    protected $count;

    /**
     * @Assert\Choice({"thumbnail", "low_resolution", "standard_resolution"})
     */
    protected $size;

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = (string) $size;
    }

    public function getTagName()
    {
        return $this->tagName;
    }
    public function setTagName($tagName)
    {
        $this->tagName = (string) $tagName;
    }

    public function getMaxId()
    {
        return $this->maxId;
    }

    public function setMaxId($maxId = 0)
    {
        $this->maxId = (integer) $maxId;
    }
}
