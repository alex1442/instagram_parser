<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class AddAccountEntity
{
    /**
     *   @Assert\Type(type="integer")
     *   @Assert\NotBlank()
     */
    protected $userId;

    /**
     *   @Assert\Type(type="boolean")
     */
    protected $activity;

    /**
     * @Assert\Length(max = 2)
     */
    protected $caption;

    public function getCaption()
    {
        return $this->caption;
    }

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setActivity($activity)
    {
        $this->activity = $activity;
    }
}
