<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InstagramTasks
 *
 * @ORM\Table(name="instagram_tasks")
 * @ORM\Entity
 */
class InstagramTasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="task_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taskId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="posting_date", type="datetime", nullable=true)
     */
    private $postingDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activity_status", type="boolean", nullable=false)
     */
    private $activityStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="text", length=65535, nullable=true)
     */
    private $caption;



    /**
     * Get taskId
     *
     * @return integer
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return InstagramTasks
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set postingDate
     *
     * @param \DateTime $postingDate
     *
     * @return InstagramTasks
     */
    public function setPostingDate($postingDate)
    {
        $this->postingDate = $postingDate;

        return $this;
    }

    /**
     * Get postingDate
     *
     * @return \DateTime
     */
    public function getPostingDate()
    {
        return $this->postingDate;
    }

    /**
     * Set activityStatus
     *
     * @param boolean $activityStatus
     *
     * @return InstagramTasks
     */
    public function setActivityStatus($activityStatus)
    {
        $this->activityStatus = $activityStatus;

        return $this;
    }

    /**
     * Get activityStatus
     *
     * @return boolean
     */
    public function getActivityStatus()
    {
        return $this->activityStatus;
    }

    /**
     * Set caption
     *
     * @param string $caption
     *
     * @return InstagramTasks
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }
}
