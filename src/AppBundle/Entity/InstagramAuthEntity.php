<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class InstagramAuthEntity
{
    /**
     *   @Assert\Type(type="string")
     *   @Assert\NotBlank()
     *   @Assert\Type(
     *     type="graph"
     * )
     */
    protected $tagName;

    /**
     *   @Assert\Type(type="integer")
     *   @Assert\GreaterThanOrEqual(
     *     value = 1
     * )
     */
    protected $count;

    public function getTagName()
    {
        return $this->tagName;
    }
    public function setTagName($tagName)
    {
        $this->tagName = (string) $tagName;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count = null)
    {
        $this->count = (integer) $count;
    }
}
