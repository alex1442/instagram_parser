<?php

use Symfony\Component\Validator\Constraints as Assert;

namespace AppBundle\Entity;

class GetVkRepostsEntity
{

    /**
     *   @Assert\Type(type="integer")
     */
    protected $vkUserId;

    /**
     *   @Assert\Type(type="integer")
     */
    protected $vkPostId;

    /**
     * @Assert\Type(type="integer")
     * @Assert\GreaterThanOrEqual(
     *     value = 1
     * )
     */
    protected $count;
    
        /**
     * @Assert\Type(type="integer")
     * @Assert\GreaterThanOrEqual(
     *     value = 1
     * )
     */
    protected $offset;
    
        
    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = (integer) $offset;
    }
    
    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }
    
    public function getVkPostId()
    {
        return $this->vkPostId;
    }

    public function setVkPostId($vkPostId)
    {
        $this->vkPostId = (integer) $vkPostId;
    }
    
    public function getVkUserId()
    {
        return $this->vkUserId;
    }

    public function setVkUserId($vkUserId)
    {
        $this->vkUserId = (integer) $vkUserId;
    }
}
