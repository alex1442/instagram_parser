<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class TagEntity
{
    /**
     *   @Assert\Type(type="string")
     *   @Assert\NotBlank()
     */
    protected $tagName;

    /**
     *   @Assert\Type(type="integer")
     */
    protected $count;

    public function getTagName()
    {
        return $this->tagName;
    }
    public function setTagName($tagName)
    {
        $this->tagName = (string) $tagName;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count = null)
    {
        $this->count = (integer) $count;
    }
}
