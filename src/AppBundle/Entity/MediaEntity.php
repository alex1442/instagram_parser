<?php

namespace AppBundle\Entity;

class MediaEntity
{
    private $id;
    private $tags;
    private $type;
    //private $comments;
    private $creationTime;
    private $username;
    private $link;
    //private $likes;
    private $images;
    private $caption;
    public function getId()
    {
        return $this->id;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCreationTime()
    {
        return $this->creationTime;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function getCaption()
    {
        return $this->caption;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
    }

    public function setTags($tags)
    {
        $this->tags = (array) $tags;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setCreationTime($creationTime)
    {
        $this->creationTime = (int) $creationTime;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function setImages($images)
    {
        $this->images = $images;
    }

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    public function __construct($mediaData)
    {
        $this->id = (string) $mediaData['id'];
        $this->username = (string) $mediaData['username'];
        $this->type = (string) $mediaData['type'];
        $this->tags = array_unique(array_map('strval', $mediaData['tags']), SORT_STRING);
        $this->creationTime = (int) $mediaData['creationTime'];
        $this->username = (string) $mediaData['username'];
        $this->link = (string) $mediaData['link'];
        $this->caption = (string) $mediaData['caption'];
        $this->images = ['thumbnail' => (string) $mediaData['images']['thumbnail'],
        'low_resolution' => (string) $mediaData['images']['low_resolution'],
        'standard_resolution' => (string) $mediaData['images']['standard_resolution'], ];
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
    }
}
