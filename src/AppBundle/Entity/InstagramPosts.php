<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InstagramPosts
 *
 * @ORM\Table(name="instagram_posts", indexes={@ORM\Index(name="fk_instagram_account_id_idx", columns={"fk_account_id"})})
 * @ORM\Entity
 */
class InstagramPosts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="post_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $postId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="using_status", type="boolean", nullable=false)
     */
    private $usingStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="availability_status", type="boolean", nullable=true)
     */
    private $availabilityStatus;

    /**
     * @var \AppBundle\Entity\InstagramAccounts
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InstagramAccounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_account_id", referencedColumnName="account_id")
     * })
     */
    private $fkAccount;



    /**
     * Get postId
     *
     * @return integer
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Set usingStatus
     *
     * @param boolean $usingStatus
     *
     * @return InstagramPosts
     */
    public function setUsingStatus($usingStatus)
    {
        $this->usingStatus = $usingStatus;

        return $this;
    }

    /**
     * Get usingStatus
     *
     * @return boolean
     */
    public function getUsingStatus()
    {
        return $this->usingStatus;
    }

    /**
     * Set availabilityStatus
     *
     * @param boolean $availabilityStatus
     *
     * @return InstagramPosts
     */
    public function setAvailabilityStatus($availabilityStatus)
    {
        $this->availabilityStatus = $availabilityStatus;

        return $this;
    }

    /**
     * Get availabilityStatus
     *
     * @return boolean
     */
    public function getAvailabilityStatus()
    {
        return $this->availabilityStatus;
    }

    /**
     * Set fkAccount
     *
     * @param \AppBundle\Entity\InstagramAccounts $fkAccount
     *
     * @return InstagramPosts
     */
    public function setFkAccount(\AppBundle\Entity\InstagramAccounts $fkAccount = null)
    {
        $this->fkAccount = $fkAccount;

        return $this;
    }

    /**
     * Get fkAccount
     *
     * @return \AppBundle\Entity\InstagramAccounts
     */
    public function getFkAccount()
    {
        return $this->fkAccount;
    }
}
