<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ListAccountsEntity
{
    /**
     *   @Assert\Type(type="string")
     */
    protected $q;
    /**
     *   @Assert\Type(type="boolean")
     */
    protected $activityStatus;

    /**
     *   @Assert\NotBlank()
     *   @Assert\Type(type="integer")
     *   @Assert\GreaterThanOrEqual(
     *     value = 1
     * )
     */
    protected $count;
    /**
     *   @Assert\NotBlank()
     *   @Assert\GreaterThanOrEqual(
     *     value = 0
     * )
     */
    protected $offset;

    public function getQ()
    {
        return $this->q;
    }

    public function getActivityStatus()
    {
        return $this->activityStatus;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setQ($q)
    {
        $this->q = $q;
    }

    public function setActivityStatus($activityStatus)
    {
        $this->activityStatus = $activityStatus;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }
}
