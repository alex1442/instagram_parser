<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Tag
{
    /**
     *   @Assert\Type(type="string")
     *   @Assert\NotBlank()
     */
    protected $name;

    /**
     *   @Assert\Type(type="integer")
     */
    protected $count;

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = (string) $name;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count = null)
    {
        $this->count = (integer) $count;
    }
}
