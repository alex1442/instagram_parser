<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class SearchMediaEntity
{
    /**
     *   @Assert\Type(type="string")
     *   @Assert\NotBlank()
     *   @Assert\Type(
     *     type="graph"
     * )
     */
    protected $tagName;

    /**
     *   @Assert\Type(type="integer")
     */
    protected $maxTagId;

    /**
     * @Assert\Type(type="integer")
     * @Assert\GreaterThanOrEqual(
     *     value = 1
     * )
     */
    protected $count;
    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }
    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Choice({"thumbnail", "low_resolution", "standard_resolution"})
     */
    protected $size = 'standard_resolution';

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = (string) $size;
    }

    public function getTagName()
    {
        return $this->tagName;
    }
    public function setTagName($tagName)
    {
        $this->tagName = (string) $tagName;
    }

    public function getMaxTagId()
    {
        return $this->maxTagId;
    }

    public function setMaxTagId($maxTagId = 0)
    {
        $this->maxTagId = (integer) $maxTagId;
    }
}
