<?php

namespace AppBundle\Entity;

class Media
{
    private $id;
    private $tags;
    private $type;
    //private $comments;
    private $creationTime;
    private $username;
    private $link;
    //private $likes;
    private $images;
    private $caption;

    public function __construct($mediaData)
    {
        $this->id = (string) $mediaData['id'];
        $this->username = (string) $mediaData['username'];
        $this->type = (string) $mediaData['type'];
        $this->tags = array_unique(array_map('strval', $mediaData['tags']), SORT_STRING);
        $this->$creationTime = (int) $mediaData['creationTime'];
        $this->username = (string) $mediaData['username'];
        $this->link = (string) $mediaData['link'];
        $this->caption = (string) $mediaData['caption'];
        $this->images = ['thumbnail' => (string) $mediaData['images']['thumbnail'],
        'low_resolution' => (string) $mediaData['images']['low_resolution'],
        'standard_resolution' => (string) $mediaData['images']['standard_resolution'], ];
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
    }
}
