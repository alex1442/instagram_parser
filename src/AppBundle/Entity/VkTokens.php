<?php

namespace AppBundle\Entity;

/**
 * VkTokens
 */
class VkTokens
{
    /**
     * @var integer
     */
    private $tokenId;

    /**
     * @var integer
     */
    private $expiresIn;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var integer
     */
    private $userId;


    /**
     * Get tokenId
     *
     * @return integer
     */
    public function getTokenId()
    {
        return $this->tokenId;
    }

    /**
     * Set expiresIn
     *
     * @param integer $expiresIn
     *
     * @return VkTokens
     */
    public function setExpiresIn($expiresIn)
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    /**
     * Get expiresIn
     *
     * @return integer
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return VkTokens
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     *
     * @return VkTokens
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return VkTokens
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
