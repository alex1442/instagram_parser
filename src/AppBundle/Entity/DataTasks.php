<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DataTasks
 *
 * @ORM\Table(name="data_tasks", indexes={@ORM\Index(name="fk_post_id_idx", columns={"fk_post_id"}), @ORM\Index(name="fk_task_id_idx", columns={"fk_task_id"})})
 * @ORM\Entity
 */
class DataTasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="data_task_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $dataTaskId;

    /**
     * @var \AppBundle\Entity\InstagramPosts
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InstagramPosts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_post_id", referencedColumnName="post_id")
     * })
     */
    private $fkPost;

    /**
     * @var \AppBundle\Entity\InstagramTasks
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InstagramTasks")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_task_id", referencedColumnName="task_id")
     * })
     */
    private $fkTask;



    /**
     * Get dataTaskId
     *
     * @return integer
     */
    public function getDataTaskId()
    {
        return $this->dataTaskId;
    }

    /**
     * Set fkPost
     *
     * @param \AppBundle\Entity\InstagramPosts $fkPost
     *
     * @return DataTasks
     */
    public function setFkPost(\AppBundle\Entity\InstagramPosts $fkPost = null)
    {
        $this->fkPost = $fkPost;

        return $this;
    }

    /**
     * Get fkPost
     *
     * @return \AppBundle\Entity\InstagramPosts
     */
    public function getFkPost()
    {
        return $this->fkPost;
    }

    /**
     * Set fkTask
     *
     * @param \AppBundle\Entity\InstagramTasks $fkTask
     *
     * @return DataTasks
     */
    public function setFkTask(\AppBundle\Entity\InstagramTasks $fkTask = null)
    {
        $this->fkTask = $fkTask;

        return $this;
    }

    /**
     * Get fkTask
     *
     * @return \AppBundle\Entity\InstagramTasks
     */
    public function getFkTask()
    {
        return $this->fkTask;
    }
}
